DefinitionBlock ("SSDT-USB.aml", "SSDT", 1, "ChrisB", "USBFix", 0x00003000)
// I dont need: HS05 HS06 HS07 HS08 HS09 SS07 SS08 
{
    // "USBInjectAllConfiguration" : override settings for USBInjectAll.kext
    Device(UIAC)
    {
        Name(_HID, "UIA00000")
        // "ChrisBConFiguration"
        Name(CBCF, Package()
        {
            // XHC overrides for 100-series boards
            "8086_a12f", Package()
            {
                "port-count", Buffer() { 0x18, 0, 0, 0}, // Highest port number is SS08 at 0x18
                "ports", Package()
                {   "HS01", Package() // USB2 device on USB3 bottom port above right type-c, port <01 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x01, 0, 0, 0 },
                    },
                    "HS02", Package() // USB2 device on USB3 top port above right type-c, port <02 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x02, 0, 0, 0 },
                    },
                    "HS03", Package() // USB2 device on USB3 bottom port above left type-c, port <03 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x03, 0, 0, 0 },
                    },
                    "HS04", Package() // USB2 device on USB3 top port above left type-c, port <04 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x04, 0, 0, 0 },
                    },
                    "HS05", Package() // USB2 #1 from USB3 motherboard header 1, port <05 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x05, 0, 0, 0 },
                    },
                    "HS06", Package() // USB2 #2 from USB3 motherboard header 1, port <06 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x06, 0, 0, 0 },
                    },
                    "HS07", Package() // USB2 #1 from USB3 motherboard header 2, port <07 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x07, 0, 0, 0 },
                    },
                    "HS08", Package() // USB2 #2 from USB3 motherboard header 2, port <08 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x08, 0, 0, 0 },
                    },
                    "HS09", Package() // USB2 from USB2 motherboard header 2, port <09 00 00 00> // OK
                    {
                        "UsbConnector", 0,
                        "port", Buffer() { 0x09, 0, 0, 0 },
                    },
                    "HS10", Package() // USB2 Hub from USB2 motherboard header 1, port <0a 00 00 00> // OK
                    {
                        "UsbConnector", 0,
                        "port", Buffer() { 0x0a, 0, 0, 0 },
                    },
                    "HS11", Package() // USB2 device on USB2 bottom port under RJ45, port <0b 00 00 00> // OK
                    {
                        "UsbConnector", 0,
                        "port", Buffer() { 0x0b, 0, 0, 0 },
                    },
                     "HS12", Package() // USB2 device on USB2 top port under RJ45, port <0c 00 00 00> // OK
                    {
                        "UsbConnector", 0,
                        "port", Buffer() { 0x0c, 0, 0, 0 },
                    },
                    "HS13", Package() // USB2 device on USB2 port above PS/2 top, port <0d 00 00 00> // OK
                    {
                        "UsbConnector", 0,
                        "port", Buffer() { 0x0d, 0, 0, 0 },
                    },
                    "HS14", Package() // USB2 device on USB2 port above PS/2 bottom, port <0e 00 00 00> // OK
                    {
                        "UsbConnector", 0,
                        "port", Buffer() { 0x0e, 0, 0, 0 },
                    },
                    "SS01", Package() // USB3 device on USB3 bottom port above right type-c port <11 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x11, 0, 0, 0 },
                    },
                    "SS02", Package() // USB3 device on USB3 top port above right type-c port <12 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x12, 0, 0, 0 },
                    },
                    "SS03", Package() // USB3 device on USB3 bottom port above left type-c port <13 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x13, 0, 0, 0 },
                    },
                    "SS04", Package() // USB3 device on USB3 top port above left type-c port <14 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x14, 0, 0, 0 },
                    },
                    "SS05", Package() // USB3 #1 from USB3 motherboard header 1, port <15 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x15, 0, 0, 0 },
                    },
                    "SS06", Package() // USB3 #2 from USB3 motherboard header 1, port <16 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x16, 0, 0, 0 },
                    },
                    "SS07", Package() // USB3 #1 from USB3 motherboard header 2, port <17 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x17, 0, 0, 0 },
                    },
                    "SS08", Package() // USB3 #2 from USB3 motherboard header 2, port <18 00 00 00> // OK
                    {
                        "UsbConnector", 3,
                        "port", Buffer() { 0x18, 0, 0, 0 },
                    },
                },
            },
        })
    }
}
