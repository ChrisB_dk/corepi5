## GIT

### Config stuff
```
git config --global user.name "Biorn Christiansen" && \
git config --global user.email "bc@chrisb.dk" && \
git config --global github.user ChrisBlow && \
git config --global core.editor "atom-beta" && \
git config --global color.ui true && \
git config --global push.default simple
```
