## [ INSTALL ] ###

### Install MySQL ###
```
brew install mysql
```

### Install PHP ###
```
brew tap homebrew/services && brew tap homebrew/dupes && brew tap homebrew/php
brew install php70 --with-fpm --with-mysql --with-opcache --with-xdebug --without-apache
```

### Install Nginx ###
```
brew install nginx
```

### Start & Stop ###
```
# MySQL
mysql.server start / stop

# PHP
php-fpm --fpm-config /usr/local/etc/php/7.0/php-fpm.conf -D
killall -9 php-fpm

# Nginx
sudo nginx / -s stop
```

### Setup Deamon ###
```
brew services start mysql
brew services start php70
brew services start nginx
```

change PATH
```
export PATH="/usr/local/bin:/usr/local/sbin:$PATH"
```

### Configure ###
```
atom /usr/local/etc/nginx/nginx.conf
```


### Check services ###
```
# MySQL
mysql -u root -e 'STATUS'

# PHP
lsof -Pni4 | grep LISTEN | grep php

# Nginx
curl --head http://localhost  
```
