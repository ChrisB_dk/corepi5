## [ README ] ##

### Metadata Files ###
Disable creation of metadata files on network volumes
```
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
```
Disable creation of metadata files on usb volumes
```
defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true
```

### Find ###
Recursively Delete .DS_Store Files
```
find . -type f -name '*.DS_Store' -ls -delete
```

### Root user ###
```
# Enable
dsenableroot

# Disable
dsenableroot -d
```

### stop asking ###
```
sudo nano /etc/sudoers
```
add
```
<user> ALL=(ALL) NOPASSWD: ALL
```
where <user> can be a group %admin or a single user

### Xcode ###
Install Command Line Tools without Xcode
```
xcode-select --install
```

### Homebrew ###
Install
```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

brew doctor
brew update
brew upgrade
```
