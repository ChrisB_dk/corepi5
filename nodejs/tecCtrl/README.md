# tecCtrl

This controls the peltier/tec elements, that is the termoelectric voodoo in the chiller.

[Here](https://github.com/hekike/liquid-pid) is a PID controller for liquids ( beer, water, more beer )
Read the [wiki](https://en.wikipedia.org/wiki/PID_controller) for basic understanding of PID
REX controls is a nice developer tool for PID. And the bonus part it, that the software and simulator runs on Raspberry Pi, and supports alot of different I/Os. So go get it [here](https://www.rexcontrols.com/)
