# CorePi5

[![author](https://img.shields.io/badge/author-ChrisB-ff9933.svg)](https://bitbucket.org/ChrisB_dk)
[![Website](https://img.shields.io/website-online-offline-ff9933-red/https/chrisb.dk.svg?label=chrisb.dk)](https://chrisb.dk)
[![lal-1.3](https://img.shields.io/badge/license-lal--1.3-brightgreen.svg)](https://bitbucket.org/ChrisB_dk/corepi5/raw/6642b87857953fb6ccac607af95388f402adbba7/LICENSE) [![cc-by-sa-40](https://img.shields.io/badge/cc--by--sa-4.0-brightgreen.svg)](https://bitbucket.org/ChrisB_dk/corepi5/raw/6642b87857953fb6ccac607af95388f402adbba7/LICENSE)


I have begun a new build, and its my first build since the 90' ... Its a replacement for my old Apple Mac Mini, that died on me some weeks ago. Then I look a the prices, and went for a Hackintosh build. (And there went my savings for the trip to New York) Its a workstation build, not a gaming rig, for that I have my consoles.

### Hardware v.1.0
  - Intel Core i7-6700K 4.0GHz @ **4.3GHz** Quad-Core Processor
  - ~~Enermax LiqMax II 240 AIO Liquid CPU cooler~~
  - Gigabyte GA-Z170X-UD5 TH ATX LGA1151 Motherboard
  - G.Skill TridentZ Series 16GB (2 x 8GB) DDR4-3200 Memory
  - Corsair RM 550W 80+ Gold Certified Fully-Modular ATX Power Supply
  - Thermaltake Core P5 ATX Full Tower Case

### What's next?
I'm planning to make a Raspberry Pi 3 the little helper that can do magic. The schematic and PCB stuff will be uploaded here, just like the NodeJS source code. 
**Here is some magic:**
  - Watchdog
  - Fan controller
  - Fan spoofing
  - TEC controller
  - Temperature sensing
  - RGB Leds controller
  - External Display with info

### For the eyes
Some stuff I need to change, because the better it looks, the more it will be seen.
~~Coverplate on Enermax pump/head is silver, needs to be gold.~~
Some plates on the G.Skill memory is red, needs to be gold.

## License

[lal-1.3 license](https://raw.githubusercontent.com/ChrisB_dk/CorePi5/master/LICENSE) @[ChrisB_dk](https://bitbucket.org/ChrisB_dk)
[cc-by-sa-40 license](https://raw.githubusercontent.com/ChrisB_dk/CorePi5/master/LICENSE) @[ChrisB_dk](https://bitbucket.org/ChrisB_dk)

## Contact

  *   [![Email](https://img.shields.io/badge/mailto-ChrisB_dk-ff9933.svg)](mailto:bc@chrisb.dk)
  *   [![Bitcucket](https://img.shields.io/badge/contact-Bitbucket-0149b1.svg)](https://bitbucket.org/ChrisB_dk)
  *   [![Twitter](https://img.shields.io/twitter/url/https/ChrisB_dk.svg?style=social)](https://twitter.com/ChrisB_dk)

