# GLASS UPDATE

Hej glass fagkyndige.

Jeg kunne godt tænke mig et tilbud på et stykke med glas.

Og lidt mere specifikt:
  - 5mm hærdet klar glas (+/- 1mm)
  - 570,0mm x 570,0mm
  - 10,0mm afrundede hjørner
  - Poleret / Slebet kandt (så man ikke skære sig)
  - 8,0mm hul placeret 15,0mm fra kandterne i alle 4 hjørner

Se evt påklistrede PDF og/eller SVG for tegning.
Hvis der skulle være nogen tvivl, eller andet jeg kan svare eller gøre, så kan jeg kontaktes på nedstående.

Hvad skal sådan en koste, og hvad er leverings tiden?


Venlig hilsen

Bjørn Christiansen
ChrisB.dk
Bredstedgade 6 st th
DK5000 Odense C
Tlf: 22666693


---


### ~~Den Blå Glarmester~~
Store Glasvej 38
5000 Odense C
Tlf: 66144840
Email: info@blaa-glarmester.dk


### ~~Glas-Direkt.dk~~
Niels Bohrs Alle 181
5220 Odense SØ
Tlf: 7020 7533
Email: info@glas-direkt.dk

### ~~Fyns Glarmester - og tømrer ApS~~
Søren Eriksens Vej 15
5270 Odense N
Tlf: 66165844
Email: post@fynsglarmester.dk 


### ~~Ega Glas~~
Oluf Bagers Gade 3
5000 Odense C​
​Tlf: 66149344
Email: jess@egaglas.dk


### ~~Thykjær Glarmester & tømrer ApS~~
Sivlandvænget 9 B Hjallese
5260 Odense S
Tlf: 66135565
Email: info@thykjaer-jt.dk


### ~~Redtz~~
Niels Bohrs Allé 181
5220 Odense Sø
Tlf: 66147913
Email: info@redtz.dk


### ~~CORDES GLAS~~
Cikorievej 96
5220 Odense SØ
Tlf: 6617 7449
Email: mail@cordesglas.dk


